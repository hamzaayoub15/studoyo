import "./App.css";
import React from "react";
import CommonProvider from "./contexts/Common";
import ProductsProvider from "./contexts/Products";
import CartProvider from "./contexts/Cart";
import Header from "./components/Header";
import Home from "./pages/Home";
const App = () => {
  return (
    <CommonProvider>
      <ProductsProvider>
        <CartProvider>
          <div className="app">
            <Header />
            <Home />
          </div>
        </CartProvider>
      </ProductsProvider>
    </CommonProvider>
  );
};

export default App;
