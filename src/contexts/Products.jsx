import React, { useReducer, createContext } from "react";

const initialState = {
  products: null,
  isLoading: false,
  isLoaded: false,
};

export const ProductsStateContext = createContext();
export const ProductsDispatchContext = createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case "GET_PRODUCTS_REQUEST":
      return {
        ...state,
        isLoading: true,
        isLoaded: false,
      };
    case "GET_PRODUCTS_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isLoaded: true,
        products: action.payload.products,
      };
    case "GET_PRODUCTS_FAILURE":
      return {
        ...state,
        products: null,
        isLoading: false,
        isLoaded: false,
      };
    default:
      throw new Error(`Unknown action: ${action.type}`);
  }
};

const ProductsProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <ProductsDispatchContext.Provider value={dispatch}>
      <ProductsStateContext.Provider value={state}>
        {children}
      </ProductsStateContext.Provider>
    </ProductsDispatchContext.Provider>
  );
};

export const getProducts = (dispatch) => {
  dispatch({
    type: "GET_PRODUCTS_REQUEST",
  });
  dispatch({
    type: "GET_PRODUCTS_SUCCESS",
    payload: {
      products: [
        {
          id: 1,
          name: "Brocolli - 1 Kg",
          price: 120,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620046/dummy-products/broccoli.jpg",
          category: "vegetables",
        },
        {
          id: 2,
          name: "Cauliflower - 1 Kg",
          price: 60,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620046/dummy-products/cauliflower.jpg",
          category: "vegetables",
        },
        {
          id: 3,
          name: "Cucumber - 1 Kg",
          price: 48,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620046/dummy-products/cucumber.jpg",
          category: "vegetables",
        },
        {
          id: 4,
          name: "Beetroot - 1 Kg",
          price: 32,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/beetroot.jpg",
          category: "vegetables",
        },
        {
          id: 5,
          name: "Carrot - 1 Kg",
          price: 56,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620046/dummy-products/carrots.jpg",
          category: "vegetables",
        },
        {
          id: 6,
          name: "Tomato - 1 Kg",
          price: 16,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/tomato.jpg",
          category: "vegetables",
        },
        {
          id: 7,
          name: "Beans - 1 Kg",
          price: 82,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/beans.jpg",
          category: "vegetables",
        },
        {
          id: 8,
          name: "Brinjal - 1 Kg",
          price: 35,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/brinjal.jpg",
          category: "vegetables",
        },
        {
          id: 9,
          name: "Capsicum",
          price: 60,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/capsicum.jpg",
          category: "vegetables",
        },
        {
          id: 10,
          name: "Mushroom - 1 Kg",
          price: 75,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/button-mushroom.jpg",
          category: "vegetables",
        },
        {
          id: 11,
          name: "Potato - 1 Kg",
          price: 22,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/potato.jpg",
          category: "vegetables",
        },
        {
          id: 12,
          name: "Pumpkin - 1 Kg",
          price: 48,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/pumpkin.jpg",
          category: "vegetables",
        },
        {
          id: 13,
          name: "Corn - 1 Kg",
          price: 75,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/corn.jpg",
          category: "vegetables",
        },
        {
          id: 14,
          name: "Onion - 1 Kg",
          price: 16,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/onion.jpg",
          category: "vegetables",
        },
        {
          id: 15,
          name: "Apple - 1 Kg",
          price: 72,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/apple.jpg",
          category: "fruits",
        },
        {
          id: 16,
          name: "Banana - 1 Kg",
          price: 45,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/banana.jpg",
          category: "fruits",
        },
        {
          id: 17,
          name: "Grapes - 1 Kg",
          price: 60,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/grapes.jpg",
          category: "fruits",
        },
        {
          id: 18,
          name: "Mango - 1 Kg",
          price: 75,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/mango.jpg",
          category: "fruits",
        },
        {
          id: 19,
          name: "Musk Melon - 1 Kg",
          price: 36,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/musk-melon.jpg",
          category: "fruits",
        },
        {
          id: 20,
          name: "Orange - 1 Kg",
          price: 75,
          image:
            "https://res.cloudinary.com/sivadass/image/upload/v1493620045/dummy-products/orange.jpg",
          category: "fruits",
        },
      ],
    },
  });
};

export default ProductsProvider;
