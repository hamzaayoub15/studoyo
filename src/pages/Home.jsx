import React, { useEffect, useContext } from "react";
import ProductCard from "../components/ProductCard";
import {
  ProductsStateContext,
  ProductsDispatchContext,
  getProducts,
} from "../contexts/Products";
import { CommonStateContext } from "../contexts/Common";
import { Grid } from "@mui/material";

const Home = () => {
  const { products, isLoading, isLoaded } = useContext(ProductsStateContext);
  const { searchKeyword } = useContext(CommonStateContext);
  const dispatch = useContext(ProductsDispatchContext);

  const productsList =
    products &&
    products.filter((product) => {
      return (
        product.name.toLowerCase().includes(searchKeyword.toLowerCase()) ||
        !searchKeyword
      );
    });

  useEffect(() => {
    getProducts(dispatch);
  }, []);

  if (isLoading) {
    return (
      <Grid className="products-wrapper">
        <h1>Loading...</h1>
      </Grid>
    );
  }
  return (
    <Grid className="products-wrapper">
      <Grid container spacing={2} className="card">
        {isLoaded &&
          productsList.map((data) => {
            return (
              <Grid item md={3} xs={6} key={data.id} className="card mt-2">
                <ProductCard data={data} />
              </Grid>
            );
          })}
      </Grid>
    </Grid>
  );
};

export default Home;
