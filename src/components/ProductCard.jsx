import React, { useState, useContext } from "react";
import { CartDispatchContext, addToCart } from "../contexts/Cart";
import Card from "@mui/material/Card";

import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Divider, Grid } from "@mui/material";
import ViewImageDialog from "./ViewProduct";
import "./../App.css";

export default function ProductCard(props) {
  const [isAdded, setIsAdded] = useState(false);
  const dispatch = useContext(CartDispatchContext);
  const [openProduct, setOpenPtoduct] = useState(false);

  const handleAddToCart = () => {
    const product = { ...props.data, quantity: 1 };
    addToCart(dispatch, product);
    setIsAdded(true);
    setTimeout(() => {
      setIsAdded(false);
    }, 3500);
  };
  return (
    <Card sx={{ maxWidth: 345 }} className="product-card">
      <Grid container spacing={2}>
        <Grid item md={12} xs={12}>
          <img
            onClick={() => setOpenPtoduct(true)}
            className="image"
            src={props.data.image}
            alt={props.data.image}
          ></img>
        </Grid>
        <Grid item md={12} xs={12}>
          <Divider />
        </Grid>
        <Grid item md={12} xs={12} className="card">
          <Typography>{props.data.name}</Typography>
        </Grid>
        <Grid item md={12} xs={12} className="card">
          <Typography>
            <b>$&nbsp;{props.data.price}</b>
          </Typography>
        </Grid>
        <Grid item md={12} xs={12}>
          <Button
            className={!isAdded ? "" : "added"}
            type="button"
            onClick={handleAddToCart}
            variant="contained"
            fullWidth
          >
            {!isAdded ? "ADD TO CART" : "✔ ADDED"}
          </Button>
        </Grid>
      </Grid>
      {openProduct && (
        <ViewImageDialog
          src={props.data.image}
          open={openProduct}
          setOpen={setOpenPtoduct}
          product={props.data}
        />
      )}
    </Card>
  );
}
