import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import Slide from "@mui/material/Slide";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});
export default function ViewImageDialog(props) {
  const { open, setOpen, src, product } = props;

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
        keepMounted
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogContent style={{ height: "100%" }}>
          <img width="100%" height="100%" src={src} alt={src}></img>
          <span className="prod-desc">
            <strong>{product.name}</strong>

            <p>
              <strong>$&nbsp;{product.price}</strong>
            </p>
          </span>
        </DialogContent>
      </Dialog>
    </div>
  );
}
