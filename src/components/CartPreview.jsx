import React, { useContext } from "react";
import {
  CartStateContext,
  CartDispatchContext,
  removeFromCart,
  toggleCartPopup,
} from "../contexts/Cart";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { Divider, Grid, Typography } from "@mui/material";
import { Close } from "@mui/icons-material";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});
const CartPreview = () => {
  const { items, isCartOpen } = useContext(CartStateContext);
  const dispatch = useContext(CartDispatchContext);
  const cartTotal = items
    .map((item) => item.price * item.quantity)
    .reduce((prev, current) => prev + current, 0);
  const handleRemove = (productId) => {
    return removeFromCart(dispatch, productId);
  };

  return (
    <div>
      <Dialog
        open={isCartOpen}
        TransitionComponent={Transition}
        onClose={() => toggleCartPopup(dispatch)}
        keepMounted
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Your Cart"}</DialogTitle>
        <DialogContent>
          {items.length > 0 ? (
            items.map((product) => {
              return (
                <Grid container spacing={2} key={product.name}>
                  <Grid item xs={2} className="card">
                    <img
                      style={{ width: "100%" }}
                      src={product.image}
                      alt={product.image}
                    ></img>
                  </Grid>
                  <Grid item xs={6}>
                    <p className="font">{product.name}</p>
                    <p>$&nbsp;{items[0]?.price}</p>
                  </Grid>
                  <Grid item xs={2}>
                    <p>
                      {" "}
                      {`${product.quantity} ${
                        product.quantity > 1 ? "Nos." : "No."
                      }`}
                    </p>
                    <p className="font">
                      $&nbsp;{product.quantity * product.price}
                    </p>
                  </Grid>
                  <Grid item xs={2} className="icon mt-4">
                    <Close
                      color="warning"
                      onClick={() => handleRemove(product.id)}
                    />
                  </Grid>
                </Grid>
              );
            })
          ) : (
            <Grid container className="card">
              <img
                src="https://res.cloudinary.com/sivadass/image/upload/v1495427934/icons/empty-cart.png"
                alt="empty"
              ></img>
              <h2>Your cart is empty</h2>
            </Grid>
          )}
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Divider />
            </Grid>
            <Grid item xs={12} className="card">
              <Typography>
                <b>Total Amount:</b>&nbsp;{cartTotal}
              </Typography>
            </Grid>
          </Grid>
        </DialogContent>{" "}
      </Dialog>
    </div>
  );
};

export default CartPreview;
