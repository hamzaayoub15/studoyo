import React, { useContext } from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import Grid from "@mui/material/Grid";

import {
  CartStateContext,
  CartDispatchContext,
  toggleCartPopup,
} from "../contexts/Cart";
import { CommonDispatchContext, setSearchKeyword } from "../contexts/Common";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import CartPreview from "./CartPreview";
const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function Header() {
  const { items: cartItems } = useContext(CartStateContext);
  const commonDispatch = useContext(CommonDispatchContext);
  const cartDispatch = useContext(CartDispatchContext);

  const cartQuantity = cartItems.length;
  const cartTotal = cartItems
    .map((item) => item.price * item.quantity)
    .reduce((prev, current) => prev + current, 0);

  const handleSearchInput = (event) => {
    return setSearchKeyword(commonDispatch, event.target.value);
  };

  const handleCartButton = (event) => {
    event.preventDefault();
    return toggleCartPopup(cartDispatch);
  };

  return (
    <>
      <AppBar position="static" className="header">
        <Grid container className="w-100" spacing={1}>
          <Grid item md={4} xs={4}>
            <Typography
              variant="h4"
              noWrap
              component="div"
              sx={{ display: { sm: "block" } }}
            >
              Studoyo
            </Typography>
          </Grid>
          <Grid item md={4} xs={6}>
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Search…"
                inputProps={{ "aria-label": "search" }}
                onChange={handleSearchInput}
              />
            </Search>
          </Grid>
          <Grid md={2} sx={{ display: { xs: "none", sm: "block" } }}>
            <table>
              <tbody>
                <tr>
                  <td>No. of items</td>
                  <td>:</td>
                  <td>
                    <strong>{cartQuantity}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Sub Total</td>
                  <td>:</td>
                  <td>
                    <strong>{cartTotal}</strong>
                  </td>
                </tr>
              </tbody>
            </table>
          </Grid>
          <Grid md={1} xs={2} className="card">
            <a href="#" onClick={handleCartButton}>
              <ShoppingCartIcon
                style={{
                  color: "white",
                  fontSize: "50px",
                }}
              />
              {cartQuantity ? (
                <span className="badge badge-warning" id="lblCartCount">
                  {" "}
                  {cartQuantity}{" "}
                </span>
              ) : (
                ""
              )}
            </a>
          </Grid>
        </Grid>{" "}
      </AppBar>
      <CartPreview />
    </>
  );
}
