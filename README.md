# React Shopping Cart

## Live Demo

https://studoyo.vercel.app/

## Setup

Goto the project folder and install required dependencies:

```
npm install
```

And run Webpack to watch for code changes and bundle js and scss files:

```
npm start
```

Project will be automatically open at http://localhost.com:3000

For production build:

```
npm run build
```

## Approach

.Boiler plate is built using create-react-app
.Context Library is used to manage project states,cart and filters
.Material Ui is used as Ui library
.Common css is defined in app.css folder using classes

## Challanges

Major challange faced while developing this project was managing state without using context the states was getting complex in cart preview ,calculating sum ,filtered products to overcome this challange react context library is ued to manage state at one place.
